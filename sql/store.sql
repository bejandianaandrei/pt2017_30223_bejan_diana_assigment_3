CREATE DATABASE  IF NOT EXISTS Store;
USE Store;


CREATE TABLE IF NOT EXISTS Client(
clientID VARCHAR(40) NOT NULL PRIMARY KEY,
firstname VARCHAR(40) NOT NULL,
lastname VARCHAR(40) NOT NULL,
balance  INT UNSIGNED,
phone VARCHAR(40) NOT NULL);



CREATE TABLE IF NOT EXISTS Product(
productID VARCHAR(40) NOT NULL PRIMARY KEY,
description VARCHAR(40),
price INT UNSIGNED,
valability DATE);

CREATE TABLE IF NOT EXISTS Reserve(
productID VARCHAR(40),
quantity INT UNSIGNED 
);



CREATE TABLE IF NOT EXISTS Orders(
orderID VARCHAR(40)  NOT NULL,
productID VARCHAR(40) NOT NULL,
clientID VARCHAR(40) NOT NULL,
quantity INT UNSIGNED,
totalPrice INT UNSIGNED,
placementDate DATE,
deliveryDate DATE,
canceled BOOLEAN DEFAULT FALSE,
FOREIGN KEY (clientID) REFERENCES Client(clientID),
FOREIGN KEY (productID) REFERENCES Product(productID));

ALTER TABLE Orders
ADD PRIMARY KEY(orderID, clientID, productID);
ALTER TABLE Reserve
ADD foreign key(productID) references Product(productID);








