package Store.dao;

import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

/**
 * Created by diana on 17.04.2017.
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    private final Class<T> type;

    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedTypeImpl) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();

        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }
//    public List<T> selectAll(){
//        String query = "Select * from "+type.getSimpleName()+" ;";
//        Connection connection = null;
//        ResultSet resultSet=null;
//        PreparedStatement statement=null;
//        try {
//            connection=ConnectionFactory.getConnection();
//            statement=connection.prepareStatement(query);
//            statement.execute(query);
//            resultSet=statement.executeQuery(query);
//        }
//        catch (SQLException e){
//            e.printStackTrace();
//            JOptionPane.showMessageDialog(null, "Select Data failed", null, JOptionPane.ERROR_MESSAGE);
//        }
//        finally {
//            ConnectionFactory.close(resultSet);
//            ConnectionFactory.close(statement);
//            ConnectionFactory.close(connection);
//        }
//
//        return createObjects(resultSet);
//    }


    public boolean insert(T t) throws IllegalAccessException, InvocationTargetException {
        //TODO
        return false;
    }

    public boolean update(T t) throws IllegalAccessException {
        //TODO
        return false;
    }

    public boolean delete(T t) {
        //TODO
        return false;
    }

}
