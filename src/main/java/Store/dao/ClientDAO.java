package Store.dao;

import Store.connection.ConnectionFactory;
import Store.model.Client;

import javax.swing.*;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by diana on 20.04.2017.
 */
public class ClientDAO extends AbstractDAO<Client> {

    public boolean insert(Client client) throws IllegalAccessException {
        boolean status = true;
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        String query = String.format(" INSERT INTO %s (%s, %s, %s, %s, %s) VALUES ( ?, ?, ?, ?, ? )", client.getClass().getSimpleName(), client.getClass().getDeclaredFields()[0].getName(), client.getClass().getDeclaredFields()[1].getName(), client.getClass().getDeclaredFields()[2].getName(), client.getClass().getDeclaredFields()[3].getName(), client.getClass().getDeclaredFields()[4].getName());

        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, client.getClientID().toString());
            statement.setString(2, client.getFirstname().toString());
            statement.setString(3, client.getLastname().toString());
            statement.setInt(4, client.getBalance());
            statement.setString(5, client.getPhone().toString());
            try {
                System.out.println(query + client.getClientID());

                statement.execute();
            } catch (SQLException e) {
                status = false;
                JOptionPane.showMessageDialog(null, e.getMessage(), null, JOptionPane.ERROR_MESSAGE);
            } finally {
                ConnectionFactory.close(statement);
                ConnectionFactory.close(connection);
            }
            return status;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;

    }

    public List<Client> selectAll() {
        String query = "Select * from " + Client.class.getSimpleName() + ";";

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Client> clients = new ArrayList<Client>();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            // System.out.print(statement.toString());
            resultSet = statement.executeQuery(query);

            clients = createObjects(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Select Data failed", null, JOptionPane.ERROR_MESSAGE);
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return clients;
    }

    public boolean delete(int i) {
        boolean status = true;
        Connection connection = null;
        PreparedStatement statement = null;
        Client client = selectAll().get(i);
        String query = " delete from " + client.getClass().getSimpleName() + " where clientID= ? ";
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, client.getClientID());
            System.out.print(statement.toString());
            statement.execute();
        } catch (SQLException e) {
            status = false;
            JOptionPane.showMessageDialog(null, e.getMessage(), null, JOptionPane.ERROR_MESSAGE);
        } finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);

        }
        return status;
    }

    private List<Client> createObjects(ResultSet resultSet) {
        List<Client> list = new ArrayList<Client>();

        try {


            while (resultSet.next()) {
                Client instance = new Client(resultSet.getString(1), resultSet.getString(2)
                        , resultSet.getString(3), resultSet.getInt(4), resultSet.getString(5));
                list.add(instance);
                System.out.print(resultSet.toString());
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);

        }
        return list;
    }

    public Client selectById(String id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("clientID");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, id);

            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, Client.class.getName() + "DAO:findById " + e.getMessage());
        }
        return null;
    }

    public boolean update(Client client) {

        boolean status = true;
        Connection connection;
        PreparedStatement preparedStatement = null;
        Class type = client.getClass();
        String sbQry;
        Field fields[] = type.getDeclaredFields();
        connection = ConnectionFactory.getConnection();


        sbQry = "Update " + type.getSimpleName() + " Set " + fields[1].getName() + "= ? ," +
                fields[2].getName() + "= ? ," + fields[3].getName() + " = ? ," + fields[4].getName() + "= ? WHERE " + fields[0].getName() + "= ? ;";

        try {
            preparedStatement = connection.prepareStatement(sbQry);
            fields[1].setAccessible(true);
            preparedStatement.setString(1, fields[1].get(client).toString());
            fields[2].setAccessible(true);
            preparedStatement.setString(2, fields[2].get(client).toString());
            fields[3].setAccessible(true);
            preparedStatement.setString(3, fields[3].get(client).toString());
            fields[4].setAccessible(true);
            preparedStatement.setInt(4, Integer.parseInt(fields[4].get(client).toString()));
            fields[0].setAccessible(true);
            preparedStatement.setString(5, fields[0].get(client).toString());
            System.out.println("\n" + preparedStatement.toString());
            preparedStatement.executeUpdate();


        } catch (SQLException e) {
            // e.printStackTrace();
        } catch (IllegalAccessException e) {
            // e.printStackTrace();
        } catch (IllegalArgumentException e) {
            //  e.printStackTrace();
        } finally {
            System.out.println("\n" + preparedStatement.toString());
            ConnectionFactory.close(connection);
            ConnectionFactory.close(preparedStatement);
        }


        return status;
    }
}
