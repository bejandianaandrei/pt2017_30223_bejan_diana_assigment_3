package Store.dao;

import Store.connection.ConnectionFactory;
import Store.model.Orders;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by diana on 21.04.2017.
 */
public class OrderDAO extends AbstractDAO<Orders> {
    public List<Orders> selectAll() {
        String query = "Select * from " + Orders.class.getSimpleName() + ";";

        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Orders> orders = new ArrayList<Orders>();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            System.out.print(statement.toString());
            resultSet = statement.executeQuery(query);

            orders = createObjects(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Select Data failed", null, JOptionPane.ERROR_MESSAGE);
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }

        return orders;
    }

    private List<Orders> createObjects(ResultSet resultSet) {
        List<Orders> list = new ArrayList<Orders>();

        try {


            while (resultSet.next()) {
                Orders instance = new Orders(resultSet.getString(1), (new ProductDAO()).selectById(resultSet.getString(2)),
                        (new ClientDAO()).selectById(resultSet.getString(3))
                        , resultSet.getInt(4));
                list.add(instance);
                System.out.print(resultSet.toString());
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);

        }
        return list;
    }

    public boolean insert(Orders order) throws IllegalAccessException {
        boolean status = true;
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        String query = String.format(" INSERT INTO %s (%s, %s, %s, %s, %s) VALUES ( ?, ?, ?, ?, ? )", order.getClass().getSimpleName(), order.getClass().getDeclaredFields()[0].getName(), order.getClass().getDeclaredFields()[1].getName(), order.getClass().getDeclaredFields()[2].getName(), order.getClass().getDeclaredFields()[3].getName(), order.getClass().getDeclaredFields()[4].getName());

        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, order.getOrderID().toString());
            statement.setString(3, order.getClientID().toString());
            statement.setString(2, order.getProductID().toString());
            statement.setInt(4, order.getQuantity());
            statement.setInt(5, order.getTotalPrice());
            try {
                System.out.print("\n" + statement.toString());
                statement.execute();
            } catch (SQLException e) {
                status = false;
                JOptionPane.showMessageDialog(null, e.getMessage(), null, JOptionPane.ERROR_MESSAGE);
            } finally {
                ConnectionFactory.close(statement);
                ConnectionFactory.close(connection);
            }
            return status;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;

    }

}
