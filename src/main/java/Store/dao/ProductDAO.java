package Store.dao;

import Store.connection.ConnectionFactory;
import Store.model.Product;

import javax.swing.*;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by diana on 21.04.2017.
 */
public class ProductDAO extends AbstractDAO<Product> {
    public boolean insert(Product product) throws IllegalAccessException {
        boolean status = true;
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        String query = String.format(" INSERT INTO %s (%s, %s, %s, %s, %s) VALUES ( ?, ?, ?, ?, ? )", product.getClass().getSimpleName(), product.getClass().getDeclaredFields()[0].getName(), product.getClass().getDeclaredFields()[1].getName(), product.getClass().getDeclaredFields()[2].getName(), product.getClass().getDeclaredFields()[3].getName(), product.getClass().getDeclaredFields()[4].getName());
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, product.getProductID().toString());
            statement.setString(2, product.getDescription().toString());
            statement.setInt(3, product.getPrice());
            statement.setInt(4, product.getQuantity());
            Calendar calendar = Calendar.getInstance();
            statement.setDate(5, new java.sql.Date(calendar.getTime().getTime()));
            System.out.println(query + product.getProductID());
            try {
                statement.execute();

            } catch (SQLException e) {
                status = false;
                JOptionPane.showMessageDialog(null, e.getMessage(), null, JOptionPane.ERROR_MESSAGE);

            } finally {
                ConnectionFactory.close(statement);
                ConnectionFactory.close(connection);

            }
            return status;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    public List<Product> selectAll() {
        String query = "Select * from " + Product.class.getSimpleName() + ";";
        Connection connection = null;
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        List<Product> products = new ArrayList<Product>();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery(query);
            if (resultSet != null)
                products = createObjects(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Select Product Data failed", null, JOptionPane.ERROR_MESSAGE);
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return products;
    }

    public boolean update(Product product) {
        System.out.print("\n i am" + product.getDescription());
        boolean status = true;
        Connection connection;
        PreparedStatement preparedStatement = null;
        Class type = product.getClass();
        String sbQry;
        Field fields[] = type.getDeclaredFields();
        connection = ConnectionFactory.getConnection();
        sbQry = "Update " + type.getSimpleName() + " Set " + fields[1].getName() + "= ? ," +
                fields[2].getName() + "= ? ," + fields[3].getName() + " = ? , " + fields[4].getName() + "= ? WHERE " + fields[0].getName() + "= ? ;";
        try {
            preparedStatement = connection.prepareStatement(sbQry);
            fields[1].setAccessible(true);
            preparedStatement.setString(1, fields[1].get(product).toString());
            fields[2].setAccessible(true);
            preparedStatement.setInt(2, Integer.parseInt(fields[2].get(product).toString()));
            fields[3].setAccessible(true);
            preparedStatement.setInt(3, Integer.parseInt(fields[3].get(product).toString()));
            fields[4].setAccessible(true);
            preparedStatement.setDate(4, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            fields[0].setAccessible(true);
            preparedStatement.setString(5, fields[0].get(product).toString());
            System.out.println("\n" + preparedStatement.toString());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            // e.printStackTrace();
        } catch (IllegalAccessException e) {
            // e.printStackTrace();
        } catch (IllegalArgumentException e) {
            //  e.printStackTrace();
        } finally {
            System.out.println("\n" + preparedStatement.toString());
            ConnectionFactory.close(connection);
            ConnectionFactory.close(preparedStatement);
        }
        return status;
    }

    public boolean delete(int i) {
        boolean status = true;
        Connection connection = null;
        PreparedStatement statement = null;
        Product product = selectAll().get(i);
        String query = " delete from " + product.getClass().getSimpleName() + " where productID= ? ";
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, product.getProductID());
            System.out.print(statement.toString());
            statement.execute();
        } catch (SQLException e) {
            status = false;
            JOptionPane.showMessageDialog(null, e.getMessage(), null, JOptionPane.ERROR_MESSAGE);
        } finally {
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        }
        return status;
    }

    private List<Product> createObjects(ResultSet resultSet) {
        List<Product> list = new ArrayList<Product>();
        try {

            if (resultSet != null)
                while (resultSet.next()) {
                    Product instance = new Product(resultSet.getString(1), resultSet.getString(2)
                            , resultSet.getInt(3), resultSet.getInt(4), resultSet.getDate(5).toString());
                    list.add(instance);
                    System.out.print(resultSet.toString());
                }
            else {
                System.out.println("\nceva");
                list.add(new Product());
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
        }
        return list;
    }

    public Product selectById(String id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("productID");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, Product.class.getName() + "DAO:findById " + e.getMessage());
        }
        return null;
    }
}
