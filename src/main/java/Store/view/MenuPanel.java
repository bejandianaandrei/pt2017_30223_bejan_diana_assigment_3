package Store.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by diana on 20.04.2017.
 */
public class MenuPanel extends JPanel {
    private JButton addClient;
    private JButton addProduct;
    private JButton addOrder;
    private JButton viewOrders;
    private JButton viewClients;
    private JButton viewProducts;

    private JButton cancelButton;

    public MenuPanel() {
        setPreferredSize(new Dimension(500, 500));
        setLayout(new GridLayout(3, 3));
        addClient = new JButton("Add Client");
        addProduct = new JButton("Add Product");
        addOrder = new JButton("Add Order");
        cancelButton = new JButton("Cancel");
        viewClients = new JButton("View Clients");
        viewOrders = new JButton("View Orders");
        viewProducts = new JButton("View Products");
        add(addClient);
        add(addOrder);
        add(addProduct);
        add(viewClients);
        add(viewOrders);
        add(viewProducts);
        add(new JLabel());
        add(cancelButton);
    }

    public static void main(String argv[]) {
        JFrame jf = new JFrame();
        jf.setSize(new Dimension(500, 500));
        jf.setResizable(false);
        jf.setLocation(0, 0);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setTitle("Title");
        jf.setLayout(new GridLayout(1, 1));
        jf.add(new MenuPanel());
        jf.setVisible(true);
    }

    public void addAddClientActionListener(ActionListener a) {
        addClient.addActionListener(a);
    }

    public void addAddOrderActionListener(ActionListener a) {
        addOrder.addActionListener(a);
    }

    public void addAddProductActionListener(ActionListener a) {
        addProduct.addActionListener(a);
    }

    public void addViewClientsActionListener(ActionListener a) {
        viewClients.addActionListener(a);
    }

    public void addViewOrdersActionListener(ActionListener a) {
        viewOrders.addActionListener(a);
    }

    public void addViewProductsActionListener(ActionListener a) {
        viewProducts.addActionListener(a);
    }

    public void addCancelButtonActionListener(ActionListener a) {
        cancelButton.addActionListener(a);
    }

}
