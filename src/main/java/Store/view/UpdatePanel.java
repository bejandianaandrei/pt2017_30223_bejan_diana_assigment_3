package Store.view;

import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by diana on 22.04.2017.
 */
public class UpdatePanel<T> extends JPanel {
    private Field[] declaredFields = null;

    private JButton cancelButton;
    private JButton confirmationButton;
    private ArrayList<JLabel> fieldsLabels;
    private ArrayList<JTextField> fieldsTextFields;
    private Class<T> type;

    public UpdatePanel(T object) {
        setPreferredSize(new Dimension(400, 500));
        cancelButton = new JButton("Cancel");
        confirmationButton = new JButton("Confirm");
        fieldsTextFields = new ArrayList<JTextField>();
        fieldsLabels = new ArrayList<JLabel>();
        setLayout(new GridLayout(0, 2));
        this.type = (Class<T>) ((ParameterizedTypeImpl) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        Class c = object.getClass();
        try {
            declaredFields = c.getDeclaredFields();

            Method method[] = c.getDeclaredMethods();
            for (int i = 0; i < declaredFields.length; i++) {
                String valoare = "";
                for (int j = 0; j < method.length; j++) {
                    if (method[j].getName().startsWith("get") && method[j].getName().toLowerCase().contains(declaredFields[i].getName().toLowerCase())) {
                        valoare = method[j].invoke(object).toString();
                    }
                }
                JLabel jl = new JLabel(declaredFields[i].getName());
                fieldsLabels.add(jl);
                JTextField jf;
                if (!declaredFields[i].getType().toString().contains("String"))
                    jf = new JTextField();
                else jf = new JFormattedTextField();
                jf.setSelectedTextColor(Color.BLUE);
                jf.setFont(new Font("Arial", Font.ITALIC, 20));
                jf.setBackground(new Color(200, 255, 255));
                jf.setText(valoare);
                if (declaredFields[i].getName().contains("ID"))
                    jf.setEditable(false);
                fieldsTextFields.add(jf);
            }
        } catch (Throwable e) {
            System.err.println(e);
        }
        for (int i = fieldsLabels.size() - 1; i >= 0; i--) {
            add(fieldsLabels.get(fieldsLabels.size() - 1 - i));
            add(fieldsTextFields.get(fieldsTextFields.size() - i - 1));
        }
        add(cancelButton);
        add(confirmationButton);
    }

    public void addConfirmationButtonActionListener(ActionListener a) {
        confirmationButton.addActionListener(a);
    }

    public void addCancelButtonActionListener(ActionListener a) {
        cancelButton.addActionListener(a);
    }

    public ArrayList<String> getFields() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (JTextField jft : fieldsTextFields)
            arrayList.add(jft.getText().toString());
        return arrayList;
    }

    public Class getCl() {
        return type;
    }
}
