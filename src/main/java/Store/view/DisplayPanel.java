package Store.view;

import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by diana on 21.04.2017.
 */

public class DisplayPanel<T> extends JPanel {

    private JTableHeader jTableHeader;
    private JTable jTable;
    private JButton deleteButton;
    private JButton editButton;
    private JButton cancelButton;
    private Class<T> type;
    private ButtonGroup buttonGroup;
    private JRadioButton option[];
    private GridBagConstraints gridBagConstraints;
    private JPanel aditional, aditional1, aditional2;

    public DisplayPanel(List<T> arrayList) {
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.ipadx = 0;
        gridBagConstraints.ipady = 0;
        setLayout(new GridBagLayout());
        aditional = new JPanel();
        aditional2 = new JPanel();
        aditional1 = new JPanel();
        Class<?> c = null;
        try {
            c = Class.forName(arrayList.get(0).getClass().getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        editButton = new JButton("Edit " + c.getSimpleName());
        deleteButton = new JButton("Delete " + c.getSimpleName());
        cancelButton = new JButton("Cancel");
        setPreferredSize(new Dimension(500, 500));
        int i = 0, j = 0, number = arrayList.size();
        option = new JRadioButton[number];


        jTableHeader = new JTableHeader();

        buttonGroup = new ButtonGroup();
        for (i = 0; i < number; i++) {
            option[i] = new JRadioButton();
            buttonGroup.add(option[i]);
            aditional2.add(option[i]);

        }


        this.type = (Class<T>) ((ParameterizedTypeImpl) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        int width = type.getDeclaredFields().length;

        jTable = new JTable(number, width);
        Object columnNames[] = new Object[width];
        Object rowData[][] = new Object[number][width];
        Field field[] = c.getDeclaredFields();
        i = 0;
        for (Field f : field) {
            f.setAccessible(true);
            columnNames[i] = f.getName();

            i++;
        }
        int k;
        Method[] m = c.getMethods();
        if (arrayList.size() != 0) {
            for (j = 0; j < arrayList.size(); j++) {
                k = 0;
                for (Field field1 : field) {
                    for (i = 0; i < m.length; i++) {
                        m[i].setAccessible(true);
                        try {
                            Object o;
                            if (m[i].getName().contains("get") && m[i].getName().toLowerCase().contains(field1.getName().toLowerCase())) {
                                if (!field1.getType().toString().contains("int"))
                                    rowData[j][k] = m[i].invoke(arrayList.get(j)).toString();
                                else
                                    rowData[j][k] = Integer.toString((Integer) m[i].invoke(arrayList.get(j)));
                                k++;
                            }
                        } catch (IllegalArgumentException e) {
                            // e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            //e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            //e.printStackTrace();
                        }
                    }
                }
            }
        }


        if (arrayList.size() > 0)
            jTable = new JTable(rowData, columnNames);

        else jTable = new JTable(null, columnNames);
        jTable.setRowHeight(27);
        aditional2.setPreferredSize(new Dimension(20, 400));
        aditional.add(jTable);
        aditional.setPreferredSize(new Dimension(450, 450));
        JScrollPane scrollPane = new JScrollPane(jTable);
        aditional.add(scrollPane, BorderLayout.CENTER);
        add(aditional, gridBagConstraints);
        gridBagConstraints.gridx++;
        add(aditional2, gridBagConstraints);
        gridBagConstraints.gridy++;
        aditional1.add(editButton);
        gridBagConstraints.gridx--;
        aditional1.add(deleteButton);

        aditional1.add(cancelButton);
        add(aditional1, gridBagConstraints);


    }

    public void addCancelButtonActionListener(ActionListener a) {
        cancelButton.addActionListener(a);
    }

    public void addEditButtonActionListener(ActionListener actionListener) {
        editButton.addActionListener(actionListener);
    }

    public void addDeleteButtonActionListener(ActionListener actionListener) {
        deleteButton.addActionListener(actionListener);
    }

    public int getOption() {
        for (int i = 0; i < option.length; i++) {
            if (option[i].isSelected())
                return i;
        }
        return -1;
    }
}
