package Store.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by diana on 20.04.2017.
 */
public class CreationPanel extends JPanel {
    private Field[] declaredFields = null;
    private Class cl;
    private JButton cancelButton;
    private JButton confirmationButton;
    private ArrayList<JLabel> fieldsLabels;
    private ArrayList<JFormattedTextField> fieldsTextFields;


    public CreationPanel(String className) {
        setPreferredSize(new Dimension(400, 500));
        cancelButton = new JButton("Cancel");
        confirmationButton = new JButton("Confirm");
        fieldsTextFields = new ArrayList<JFormattedTextField>();
        fieldsLabels = new ArrayList<JLabel>();
        setLayout(new GridLayout(0, 2));
        try {
            cl = Class.forName(className);
            declaredFields = cl.getDeclaredFields();
            Field m[] = declaredFields;
            for (int i = 0; i < m.length; i++) {

                JLabel jl = new JLabel(m[i].getName());
                fieldsLabels.add(jl);
                JFormattedTextField jf;
                if (!m[i].getType().toString().contains("String"))
                    jf = new JFormattedTextField(NumberFormat.getInstance());
                else jf = new JFormattedTextField();
                jf.setSelectedTextColor(Color.BLUE);
                jf.setFont(new Font("Arial", Font.ITALIC, 20));
                jf.setBackground(new Color(200, 255, 255));
                jf.setVerifyInputWhenFocusTarget(true);
                fieldsTextFields.add(jf);
            }
        } catch (Throwable e) {
            System.err.println(e);
        }
        for (int i = fieldsLabels.size() - 1; i >= 0; i--) {
            add(fieldsLabels.get(fieldsLabels.size() - 1 - i));
            add(fieldsTextFields.get(fieldsTextFields.size() - i - 1));
        }
        add(cancelButton);
        add(confirmationButton);
    }

    public void addConfirmationButtonActionListener(ActionListener a) {
        confirmationButton.addActionListener(a);
    }

    public void addCancelButtonActionListener(ActionListener a) {
        cancelButton.addActionListener(a);
    }

    public ArrayList<String> getFields() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (JFormattedTextField jft : fieldsTextFields)
            arrayList.add(jft.getText().toString());
        return arrayList;
    }

    public Class getCl() {
        return cl;
    }
}