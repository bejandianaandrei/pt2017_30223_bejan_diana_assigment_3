package Store.view;

import Store.model.Client;
import Store.model.Orders;
import Store.model.Product;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by diana on 20.04.2017.
 */
@SuppressWarnings("Since15")
public class GraphicController {
    private CreationPanel creationPanel;
    private MenuPanel menuPanel;
    private MainFrame mainFrame;
    private DisplayPanelClient displayPanelClient;
    private DisplayPanelProduct displayPanelProduct;
    private UpdateClientPanel updateClientPanel;
    private UpdateProductPanel updateProductPanel;
    private DisplayPanelOrder displayPanelOrder;
    private AddOrderPanel addOrderPanel;


    public GraphicController() {
        mainFrame = new MainFrame();
        mainFrame.setTitle("Store");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(600, 600);
        mainFrame.setLayout(new GridBagLayout());
        mainFrame.setLocationRelativeTo(null);
    }

    public void showMainFrame() {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        mainFrame.setVisible(true);

    }

    public void closeMainFrame() {
        mainFrame.setVisible(false);
        mainFrame.dispose();
    }

    public void showMenuPanel() {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        menuPanel = new MenuPanel();
        mainFrame.add(menuPanel);
        mainFrame.pack();
    }

    public void showCreationPanel(String className) {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        creationPanel = new CreationPanel(className);
        mainFrame.add(creationPanel);
        mainFrame.pack();
    }

    public void showDisplayPanelClient(ArrayList<Client> arrayListClient) {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        displayPanelClient = new DisplayPanelClient(arrayListClient);
        mainFrame.add(displayPanelClient);
        mainFrame.pack();
    }

    public void showDisplayPanelProduct(ArrayList<Product> arrayListProduct) {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        displayPanelProduct = new DisplayPanelProduct(arrayListProduct);
        mainFrame.add(displayPanelProduct);
        mainFrame.pack();
    }

    public void showDisplayPanelOrder(ArrayList<Orders> arrayListOrder) {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        displayPanelOrder = new DisplayPanelOrder(arrayListOrder);
        mainFrame.add(displayPanelOrder);
        mainFrame.pack();
    }

    public void showUpdateClientPanel(Client client) {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        updateClientPanel = new UpdateClientPanel(client);
        mainFrame.add(updateClientPanel);
        mainFrame.pack();
    }

    public void showUpdateProductPanel(Product product) {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        updateProductPanel = new UpdateProductPanel(product);
        mainFrame.add(updateProductPanel);
        mainFrame.pack();
    }

    public void showAddOrderPanel() {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        addOrderPanel = new AddOrderPanel();
        mainFrame.add(addOrderPanel);
        mainFrame.pack();
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }

    public CreationPanel getCreationPanel() {
        return creationPanel;
    }

    public MenuPanel getMenuPanel() {
        return menuPanel;
    }

    public DisplayPanelClient getDisplayPanelClient() {
        return displayPanelClient;
    }

    public DisplayPanelProduct getDisplayPanelProduct() {
        return displayPanelProduct;
    }

    public DisplayPanelOrder getDisplayPanelOrder() {
        return displayPanelOrder;
    }

    public UpdateProductPanel getUpdateProductPanel() {
        return this.updateProductPanel;
    }

    public UpdateClientPanel getUpdateClientPanel() {
        return this.updateClientPanel;
    }

    public AddOrderPanel getAddOrderPanel() {
        return addOrderPanel;
    }

}
