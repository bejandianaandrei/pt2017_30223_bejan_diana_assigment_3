package Store.view;

import Store.bll.ClientBLL;
import Store.bll.ProductBLL;
import Store.model.Client;
import Store.model.Orders;
import Store.model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by diana on 22.04.2017.
 */
public class AddOrderPanel extends JPanel {
    private JPanel clientPanel, clientOptionPanel, clientList;
    private JPanel productPanel, productOptionPanel, productList;
    private JPanel aditionalPanel;
    private JButton confirmation, cancel;
    private JTable clientTable;
    private JTable productTable;
    private JRadioButton clientButton[];
    private JRadioButton productButton[];
    private JLabel delails;
    private JLabel jLabels[];
    private JTextField textFields[];

    public AddOrderPanel() {
        setLayout(new GridBagLayout());
        List<Client> clients = (new ClientBLL()).selectAll();
        List<Product> products = (new ProductBLL().selectAll());
        confirmation = new JButton("Continue");
        cancel = new JButton("Cancel");
        clientPanel = new JPanel();
        productPanel = new JPanel();
        aditionalPanel = new JPanel(new GridLayout(0, 2));
        productOptionPanel = new JPanel();
        clientOptionPanel = new JPanel();
        clientList = new JPanel();
        productList = new JPanel();
        clientButton = new JRadioButton[clients.size()];
        productButton = new JRadioButton[products.size()];
        delails = new JLabel("Other details");
        jLabels = new JLabel[Orders.class.getDeclaredFields().length - 2];
        textFields = new JTextField[Orders.class.getDeclaredFields().length - 2];
        int numar = 0;
        /**
         *The fields for the order details
         */
        for (Field field : Orders.class.getDeclaredFields()) {
            if (!field.getType().getName().contains("Client") && !field.getType().getName().contains("Product")) {
                jLabels[numar] = new JLabel(field.getName());
                textFields[numar] = new JTextField();
                aditionalPanel.add(jLabels[numar]);
                aditionalPanel.add(textFields[numar]);
                numar++;
            }
        }
        aditionalPanel.add(confirmation);
        aditionalPanel.add(cancel);
        /**
         *Fill the tabel with clients information
         */
        Object columnNames[] = new Object[clients.get(0).getClass().getDeclaredFields().length];
        Object rowData[][] = new Object[clients.size()][clients.get(0).getClass().getDeclaredFields().length];
        Field field[] = clients.get(0).getClass().getDeclaredFields();
        int i = 0;
        for (Field f : field) {
            f.setAccessible(true);
            columnNames[i] = f.getName();

            i++;
        }
        int k;
        Method[] m = clients.get(0).getClass().getMethods();
        if (clients.size() != 0) {
            for (int j = 0; j < clients.size(); j++) {
                k = 0;
                for (Field field1 : field) {
                    for (i = 0; i < m.length; i++) {
                        m[i].setAccessible(true);
                        try {
                            Object o;
                            if (m[i].getName().contains("get") && m[i].getName().toLowerCase().contains(field1.getName().toLowerCase())) {
                                if (!field1.getType().toString().contains("int"))
                                    rowData[j][k] = m[i].invoke(clients.get(j)).toString();
                                else
                                    rowData[j][k] = Integer.toString((Integer) m[i].invoke(clients.get(j)));
                                k++;
                            }
                        } catch (IllegalArgumentException e) {
                            // e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            //e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            //e.printStackTrace();
                        }
                    }
                }
            }
        }
        clientTable = new JTable(rowData, columnNames);
        /**
         *Fill the tabel with product information
         */
        columnNames = new Object[products.get(0).getClass().getDeclaredFields().length];
        rowData = new Object[products.size()][products.get(0).getClass().getDeclaredFields().length];
        field = products.get(0).getClass().getDeclaredFields();
        i = 0;
        for (Field f : field) {
            f.setAccessible(true);
            columnNames[i] = f.getName();

            i++;
        }

        m = products.get(0).getClass().getMethods();
        if (products.size() != 0) {
            for (int j = 0; j < products.size(); j++) {
                k = 0;
                for (Field field1 : field) {
                    for (i = 0; i < m.length; i++) {
                        m[i].setAccessible(true);
                        try {
                            Object o;
                            if (m[i].getName().contains("get") && m[i].getName().toLowerCase().contains(field1.getName().toLowerCase())) {
                                if (!field1.getType().toString().contains("int"))
                                    rowData[j][k] = m[i].invoke(products.get(j)).toString();
                                else
                                    rowData[j][k] = Integer.toString((Integer) m[i].invoke(products.get(j)));
                                k++;
                            }
                        } catch (IllegalArgumentException e) {
                            // e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            //e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            //e.printStackTrace();
                        }
                    }
                }
            }
        }
        productTable = new JTable(rowData, columnNames);
        productList.add(productTable);
        JScrollPane scrollPane = new JScrollPane(productTable);
        productList.add(scrollPane, BorderLayout.CENTER);
        clientList.add(clientTable);
        JScrollPane scrollPane1 = new JScrollPane(clientTable);
        clientList.add(scrollPane1, BorderLayout.CENTER);
        ButtonGroup buttonGroup = new ButtonGroup();
        clientOptionPanel.add(new JLabel("     "));
        productOptionPanel.add((new JLabel("     ")));
        for (i = 0; i < clientButton.length; i++) {
            clientButton[i] = new JRadioButton();
            buttonGroup.add(clientButton[i]);

            clientOptionPanel.add(clientButton[i]);
        }
        ButtonGroup buttonGroup1 = new ButtonGroup();
        for (i = 0; i < productButton.length; i++) {
            productButton[i] = new JRadioButton();
            buttonGroup1.add(productButton[i]);

            productOptionPanel.add(productButton[i]);
        }
        GridBagConstraints g = new GridBagConstraints();
        g.gridy = 0;
        g.gridx = 0;
        productTable.setRowHeight(25);
        clientTable.setRowHeight(25);
        clientTable.setPreferredSize(new Dimension(350, 300));
        productTable.setPreferredSize(new Dimension(350, 300));
        clientOptionPanel.setPreferredSize(new Dimension(10, 300));
        productOptionPanel.setPreferredSize(new Dimension(10, 300));
        productList.setPreferredSize(new Dimension(450, 300));
        clientList.setPreferredSize(new Dimension(450, 300));
        clientPanel.setPreferredSize(new Dimension(470, 320));
        productPanel.setPreferredSize(new Dimension(470, 320));
        productPanel.add(productList);
        productPanel.add(productOptionPanel);
        clientPanel.add(clientList);
        clientPanel.add(clientOptionPanel);

        add(productPanel, g);
        g.gridx++;
        add(clientPanel, g);
        g.gridx--;
        g.gridy++;
        add(aditionalPanel, g);


    }

    public void addConfirmationButtonActionListener(ActionListener actionListener) {
        this.confirmation.addActionListener(actionListener);
    }

    public void addCancelButtonActionListener(ActionListener actionListener) {
        cancel.addActionListener(actionListener);
    }

    public ArrayList<String> getTextFields() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (JTextField jft : textFields)
            arrayList.add(jft.getText().toString());
        return arrayList;
    }

    public int getSelectedClient() {

        for (int i = 0; i < clientButton.length; i++) {
            if (clientButton[i].isSelected())
                return i;
        }
        return -1;
    }

    public int getSelectedProduct() {

        for (int i = 0; i < productButton.length; i++) {
            if (productButton[i].isSelected())
                return i;
        }
        return -1;
    }
}
