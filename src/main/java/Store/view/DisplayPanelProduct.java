package Store.view;

import Store.model.Product;

import java.util.List;

/**
 * Created by diana on 22.04.2017.
 */
public class DisplayPanelProduct extends DisplayPanel<Product> {
    public DisplayPanelProduct(List<Product> arrayList) {
        super(arrayList);
    }
}
