package Store.model;

/**
 * Created by diana on 20.04.2017.
 */
public class Orders {
    private String orderID;
    private Product productID;
    private Client clientID;
    private int quantity;
    private int totalPrice;


    public Orders() {

    }

    public Orders(String orderID, Product productID, Client clientID, int quantity) {
        this.orderID = orderID;
        this.productID = productID;
        this.clientID = clientID;
        this.quantity = quantity;
        this.totalPrice = quantity * productID.getPrice();
    }

    public Client getClientInstance() {
        return clientID;
    }

    public Product getProductInstance() {
        return productID;
    }

    public String getOrderID() {
        return this.orderID;
    }

    public String getProductID() {

        return this.productID.getProductID();
    }

    public String getClientID() {
        return this.clientID.getClientID();
    }

    public int getQuantity() {
        return quantity;
    }

    public int getTotalPrice() {
        return totalPrice;
    }


}
