package Store.model;

/**
 * Created by diana on 20.04.2017.
 */
public class Product {
    private String productID;
    private String description;
    private int price;
    private int quantity;
    private String valability;

    public Product() {

    }

    public Product(String productID, String description, int price, int quantity, String valability) {
        this.productID = productID;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.valability = valability;
    }

    public String getProductID() {
        return this.productID;
    }

    public String getDescription() {
        return this.description;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        if (price > 0) this.price = price;
    }

    public String getValability() {
        return this.valability;
    }

}
