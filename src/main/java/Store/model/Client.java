package Store.model;

/**
 * Created by diana on 20.04.2017.
 */
public class Client {
    private String clientID;
    private String firstname;
    private String lastname;
    private Integer balance;
    private String phone;

    public Client() {

    }

    public Client(String clientID, String firstname, String lastname,
                  Integer balance, String phone) {
        this.clientID = clientID;
        this.firstname = firstname;
        this.lastname = lastname;
        this.balance = balance;

        this.phone = phone;
    }

    public String getClientID() {
        return this.clientID;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public Integer getBalance() {
        return this.balance;
    }

    public String getPhone() {
        return this.phone;
    }

    public void addMonet(Integer money) {
        if (balance > 0) this.balance += money;
    }

    public void getMoney(Integer money) {
        if (this.balance >= money) this.balance -= money;
    }
}
