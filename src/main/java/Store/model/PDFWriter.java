package Store.model;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


/**
 * Created by diana on 20.04.2017.
 */
public class PDFWriter {
    private static int number = 1013;
    private static Font title = new Font("Courier", 28,
            Font.BOLD);

    public PDFWriter(String destination, Orders order) {
        File file = new File(destination);
        file.getParentFile().mkdirs();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(destination);
            PdfWriter pdfWriter = new PdfWriter(fileOutputStream);
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            Document document = new Document(pdfDocument);
            Paragraph p = new Paragraph();
            p.add(" REPORT NR A" + number);
            p.setFirstLineIndent(200.0f);
            p.setFontSize(30.0f);
            document.add(p);


            document.add(new Paragraph("        ").setFontSize(20.0f));
            document.add(new Paragraph("Dear customer," + order.getClientInstance().getFirstname() + " " +
                    order.getClientInstance().getLastname() + ",").setFontSize(20.0f));
            document.add(new Paragraph("Your order's " + order.getOrderID() + " details :").setFontSize(20.0f));
            document.add(new Paragraph("product: " + order.getProductInstance().getDescription() + "\n quantity :" +
                    order.getQuantity() + "\n total price: " + order.getTotalPrice()));
            document.add(new Paragraph(" \n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n       "));


            document.add(new Paragraph("Customer care"));
            number++;

            document.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}
