package Store.connection;

import javax.swing.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Author Bejan Diana
 *  ConnectionFactory
 *  Aceasta clasa contine toate metodele necesare pentru stabilirea conexiunii la baza
 *  de date si pentru inchiderea ei.
 *  Pentru a ascunde parola de acces la baza de date si numele de utilizator am
 *  folosit variabile de sistem pe care le accesez cu apelul System.getenv("NUME_VARIABILA")
 */
public class ConnectionFactory {

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    private ConnectionFactory() {
        loadDriver();
    }


    public static void loadDriver() {
        try {
            Class.forName("java.sql.Driver");
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "No database");
        }
    }

    private static Connection connectToDatabase() {
        Connection connection = null;


        String connectionURL = "jdbc:mysql://localhost:3306/Store";


        try {
            connection = DriverManager.getConnection(connectionURL, System.getenv("SQL_USER"), System.getenv("SQL_PASS"));
        } catch (SQLException exception) {
            JOptionPane.showMessageDialog(null, "Establising database Store.connection failed", "Error", JOptionPane.ERROR_MESSAGE);
            exception.printStackTrace();
        }
        return connection;
    }


    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Closing database Store.connection failed", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * @return conexiune la baza de date.
     */
    public static Connection getConnection() {
        return connectToDatabase();
    }


    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                Logger.getLogger(ConnectionFactory.class.getName()).log(Level.WARNING, "An error occured while trying to close the statement");
            }
        }
    }

    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                Logger.getLogger(ConnectionFactory.class.getName()).log(Level.WARNING, "An error occured while trying to close the ResultSet");
            }
        }
    }

    public static void main(String argv[]) {

        try {


            Statement stmt = ConnectionFactory.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from Client");
            while (rs.next()) {
                System.out.print(rs.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("eror");
        }


    }
}
