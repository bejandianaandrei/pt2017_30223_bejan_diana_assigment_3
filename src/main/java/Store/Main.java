package Store;

import Store.controller.ApplicationLogicController;

/**
 * My app
 */
public class Main {
    public static void main(String[] args) {
        @SuppressWarnings("unused")
        ApplicationLogicController applicationLogicController = new ApplicationLogicController();
    }
}
