package Store.controller;

import Store.bll.ClientBLL;
import Store.bll.OrderBLL;
import Store.bll.ProductBLL;
import Store.model.Client;
import Store.model.Orders;
import Store.model.PDFWriter;
import Store.model.Product;
import Store.view.GraphicController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @Author Bejan Diana
 *  Aceasta clasa cuprinde toata logica aplicatiei,  realizand conexiunea intre toate
 *  clasele ce o compun.
 */
public class ApplicationLogicController {
    private ClientBLL clientBLL;
    private ProductBLL productBLL;
    private OrderBLL orderBLL;
    private GraphicController graphicController;


    public ApplicationLogicController() {
        clientBLL = new ClientBLL();
        productBLL = new ProductBLL();
        orderBLL = new OrderBLL();
        graphicController = new GraphicController();
        displayMainFrame();
        displayMenuPanel();

    }

    private void displayMainFrame() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.showMainFrame();
            }
        });
    }

    private void displayMenuPanel() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.showMenuPanel();
                addMenuPanelAddClientButtonActionListener();
                addMenuPanelAddProductButtonActionListener();
                addMenuPanelAddOrderButtonActionListener();
                addMenuPanelViewClientsButtonActionListener();
                addMenuPanelViewProductsButtonActionListener();
                addMenuPanelViewOrdersButtonActionListener();
                addMenuPanelCancelButtonActionListener();
            }
        });
    }

    private void displayCreationPanel(final String className) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.showCreationPanel(className);
                addCreationPanelConfirmationButtonActionListener();
                addCreationPanelCancelButtonActionListener();
            }
        });
    }

    private void displayDisplayPanelClient() {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.showDisplayPanelClient((ArrayList<Client>) clientBLL.selectAll());
                addDisplayPanelClientCancelButtonActionListener();
                addDisplayPanelClientDeleteButtonActionListener();
                addDisplayPanelClientEditButtonActionListener();

            }
        });

    }

    public void displayDisplayPanelOrder() {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.showDisplayPanelOrder((ArrayList<Orders>) orderBLL.selectAll());
                addDisplayPanelOrderCancelButtonActionListener();


            }
        });

    }

    public void displayAddOrderPanel() {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.showAddOrderPanel();
                addAddOrderPanelCancelActionListener();
                addAddOrderPanelConfirmationActionListener();


            }
        });

    }

    private void displayDisplayPanelProducts() {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.showDisplayPanelProduct((ArrayList<Product>) productBLL.selectAll());
                addDisplayPanelProductCancelButtonActionListener();
                addDisplayPanelProductDeleteButtonActionListener();
                addDisplayPanelProductEditButtonActionListener();

            }
        });

    }

    private void displayEditProductPanel(final Product product) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.getMainFrame().setTitle("Edit product");
                graphicController.showUpdateProductPanel(product);
                addEditProductPanelCancelButtonActionListener();
                addEditProductPanelContinueButtonActionListener();


            }
        });


    }

    private void displayEditClientPanel(final Client client) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                graphicController.getMainFrame().setTitle("Edit product");
                graphicController.showUpdateClientPanel(client);
                addEditClientPanelCancelButtonActionListener();
                addEditClientPanelContinueButtonActionListener();


            }
        });


    }

    public void addMenuPanelAddClientButtonActionListener() {
        graphicController.getMenuPanel().addAddClientActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Add new client");
                displayCreationPanel(new Client().getClass().getName());

            }
        });
    }

    public void addMenuPanelAddProductButtonActionListener() {
        graphicController.getMenuPanel().addAddProductActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Add new product");
                displayCreationPanel(new Product().getClass().getName());

            }
        });

    }

    public void addMenuPanelAddOrderButtonActionListener() {
        graphicController.getMenuPanel().addAddOrderActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Add new order");
                displayAddOrderPanel();


            }
        });

    }

    public void addMenuPanelViewClientsButtonActionListener() {
        graphicController.getMenuPanel().addViewProductsActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                graphicController.getMainFrame().setTitle("Products");
                displayDisplayPanelProducts();

            }
        });
    }

    public void addMenuPanelViewProductsButtonActionListener() {
        graphicController.getMenuPanel().addViewClientsActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                graphicController.getMainFrame().setTitle("Clients");
                displayDisplayPanelClient();

            }
        });
    }

    public void addMenuPanelViewOrdersButtonActionListener() {
        graphicController.getMenuPanel().addViewOrdersActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                graphicController.getMainFrame().setTitle("Clients");
                displayDisplayPanelOrder();

            }
        });
    }

    public void addMenuPanelCancelButtonActionListener() {
        graphicController.getMenuPanel().addCancelButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int reply = JOptionPane.showConfirmDialog(null, "You still have to do some work here\n" +
                        "I think you should hit NO button!", "Really Quit?", JOptionPane.YES_NO_OPTION);
                if (reply == 0) {
                    JOptionPane.showMessageDialog(null, "Okaaaaay, you know what you are doing");
                    graphicController.closeMainFrame();
                }

            }
        });

    }

    public void addCreationPanelConfirmationButtonActionListener() {
        graphicController.getCreationPanel().addConfirmationButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client client = new Client();
                Product product = new Product();
                ArrayList<String> fields = graphicController.getCreationPanel().getFields();

                Class cl = graphicController.getCreationPanel().getCl();

                if (cl.getSimpleName().contains("Client")) {
                    client = clientBLL.createClient(fields);
                    clientBLL.insertClient(client);
                } else if (cl.getSimpleName().contains("Product")) {
                    product = productBLL.createProduct(fields);
                    productBLL.insertProduct(product);
                }
                graphicController.getMainFrame().setTitle("Store");
                displayMenuPanel();

            }
        });
    }


    public void addCreationPanelCancelButtonActionListener() {
        graphicController.getCreationPanel().addCancelButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Okaaaaay, you know what you are doing");
                graphicController.getMainFrame().setTitle("Store");
                displayMenuPanel();

            }
        });
    }

    public void addDisplayPanelProductCancelButtonActionListener() {
        graphicController.getDisplayPanelProduct().addCancelButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Store");
                displayMenuPanel();

            }
        });
    }

    public void addDisplayPanelProductDeleteButtonActionListener() {
        graphicController.getDisplayPanelProduct().addDeleteButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Store");
                int i = graphicController.getDisplayPanelProduct().getOption();
                productBLL.deleteProduct(i);
                displayMenuPanel();

            }
        });

    }

    public void addDisplayPanelProductEditButtonActionListener() {
        graphicController.getDisplayPanelProduct().addEditButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Store");
                int i = graphicController.getDisplayPanelProduct().getOption();

                displayEditProductPanel(productBLL.selectAll().get(i));

            }
        });
    }

    public void addDisplayPanelClientCancelButtonActionListener() {
        graphicController.getDisplayPanelClient().addCancelButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Store");
                displayMenuPanel();

            }
        });
    }

    public void addDisplayPanelClientDeleteButtonActionListener() {
        graphicController.getDisplayPanelClient().addDeleteButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Store");
                int i = graphicController.getDisplayPanelClient().getOption();
                clientBLL.deleteClient(i);
                displayMenuPanel();

            }
        });

    }

    public void addDisplayPanelClientEditButtonActionListener() {
        graphicController.getDisplayPanelClient().addEditButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Store");
                int i = graphicController.getDisplayPanelClient().getOption();
                displayEditClientPanel(clientBLL.selectAll().get(i));

            }
        });
    }

    public void addEditProductPanelCancelButtonActionListener() {
        graphicController.getUpdateProductPanel().addCancelButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                graphicController.getMainFrame().setTitle("Store");
                displayMenuPanel();

            }
        });
    }

    public void addEditProductPanelContinueButtonActionListener() {
        graphicController.getUpdateProductPanel().addConfirmationButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Store");

                ArrayList<String> prop = graphicController.getUpdateProductPanel().getFields();
                productBLL.updateProduct(productBLL.createProduct(prop));


                displayMenuPanel();

            }
        });

    }

    public void addDisplayPanelOrderCancelButtonActionListener() {
        graphicController.getDisplayPanelOrder().addCancelButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                graphicController.getMainFrame().setTitle("Store");
                displayMenuPanel();

            }
        });
    }

    public void addEditClientPanelCancelButtonActionListener() {
        graphicController.getUpdateClientPanel().addCancelButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                graphicController.getMainFrame().setTitle("Store");
                displayMenuPanel();

            }
        });
    }

    public void addEditClientPanelContinueButtonActionListener() {
        graphicController.getUpdateClientPanel().addConfirmationButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                graphicController.getMainFrame().setTitle("Store");

                ArrayList<String> prop = graphicController.getUpdateClientPanel().getFields();
                clientBLL.updateClient(clientBLL.createClient(prop));


                displayMenuPanel();

            }
        });

    }

    public void addAddOrderPanelCancelActionListener() {
        graphicController.getAddOrderPanel().addCancelButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                graphicController.getMainFrame().setTitle("Store");
                displayMenuPanel();

            }
        });
    }

    public void addAddOrderPanelConfirmationActionListener() {
        graphicController.getAddOrderPanel().addConfirmationButtonActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<String> prop = graphicController.getAddOrderPanel().getTextFields();
                int clientNumber = graphicController.getAddOrderPanel().getSelectedClient();
                int productNumber = graphicController.getAddOrderPanel().getSelectedProduct();
                if (clientNumber>=0&& productNumber>=0 && prop.get(0).length()>0&&prop.get(1).length()>0) {
                    for(Orders o :orderBLL.selectAll()){
                        if(o.getOrderID().equalsIgnoreCase(prop.get(0)))
                            return;
                    }
                    Client client = clientBLL.selectAll().get(clientNumber);
                    Product product = productBLL.selectAll().get(productNumber);
                    if (client.getBalance() >= product.getPrice() * Integer.parseInt(prop.get(1)) &&
                            product.getQuantity() >= Integer.parseInt(prop.get(1)))
                        orderBLL.insertOrder(orderBLL.createOrder(client, product, prop));
                    client.getMoney(orderBLL.createOrder(client, product, prop).getTotalPrice());
                    clientBLL.updateClient(client);
                    product.setQuantity(product.getQuantity() - orderBLL.createOrder(client, product, prop).getQuantity());
                    productBLL.updateProduct(product);
                    new PDFWriter("rep/report.pdf", orderBLL.createOrder(client, product, prop));
                    graphicController.getMainFrame().setTitle("Store");
                    displayMenuPanel();
                }

            }
        });

    }

}
