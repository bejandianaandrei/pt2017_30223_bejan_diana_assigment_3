package Store.bll;


import Store.dao.OrderDAO;
import Store.model.Client;
import Store.model.Orders;
import Store.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Order Business Logic Layer
 * Aceasta clasa reprezinta conexiunea dintre model.Orders si dao.OrderDAO,
 * Metodele din clasa apeleaza metodele de acces a bazei de date
 */
public class OrderBLL {

    private OrderDAO orderDAO;

    public OrderBLL() {

        orderDAO = new OrderDAO();
    }

    /**
     * Selecteaza toate comenzile din baza de date le pune intr-o lista
     *
     * @return ArrayList<Orders>
     */
    public List<Orders> selectAll() {
        List<Orders> cl;
        cl = orderDAO.selectAll();
        if (cl.size() == 0) {
            throw new NoSuchElementException("No Orders in database");
        }
        return cl;
    }

    /**
     * Apeleaza metoda de inserare a unei comenzi in baza de date
     * primeste ca parametru un obiect de tip Orders
     *
     * @param order
     */
    public void insertOrder(Orders order) {
        if (order.getOrderID().length() > 0)
            try {
                orderDAO.insert(order);
            } catch (IllegalAccessException iee) {
                iee.printStackTrace();
            }
    }


    /**
     * Primeste ca parametri Datele selectate din interfata si returneaza un nou obiect de
     * tip Orders.
     *
     * @param client
     * @param product
     * @param prop
     * @return
     */
    public Orders createOrder(Client client, Product product, ArrayList<String> prop) {

        Orders order = new Orders(prop.get(0), product, client, Integer.parseInt(prop.get(1)));


        return order;

    }
}
