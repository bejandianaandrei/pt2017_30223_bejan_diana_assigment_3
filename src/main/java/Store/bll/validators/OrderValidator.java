package Store.bll.validators;

import Store.model.Orders;

/**
 * Created by diana on 21.04.2017.
 */
public class OrderValidator implements Validator<Orders> {

    @Override
    public void validate(Orders order) {
        String message = new String("");
        if (order.getClientInstance().getBalance() < order.getTotalPrice())
            message.concat("Not enough money for this order");
        if (message.length() > 1)
            throw new IllegalArgumentException();
    }
}
