package Store.bll.validators;

import Store.model.Client;

/**
 * Created by diana on 20.04.2017.
 */
public class ClientValidator implements Validator<Client> {
    public void validate(Client client) {
        String message = new String("");
        if (client.getBalance() < 0) message.concat("Illegal balance argument\n");
        if (client.getClientID().equalsIgnoreCase("")) message.concat("Illegal ID \n");
        if (message.length() > 1)
            throw new IllegalArgumentException();
    }
}
