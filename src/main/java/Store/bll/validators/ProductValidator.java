package Store.bll.validators;

import Store.model.Product;

/**
 * Created by diana on 21.04.2017.
 */
public class ProductValidator implements Validator<Product> {


    @Override
    public void validate(Product product) {
        String message = new String("");
        if (product.getPrice() < 0 || product.getPrice() > 1000)
            message.concat("Price is not in admited range: (1..1000)");
        if (message.length() > 1) {
            throw new IllegalArgumentException(message);
        }
    }
}