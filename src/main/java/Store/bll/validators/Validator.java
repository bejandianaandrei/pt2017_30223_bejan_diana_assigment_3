package Store.bll.validators;

/**
 * Created by diana on 20.04.2017.
 */
public interface Validator<T> {
    public void validate(T t);
}