package Store.bll;

import Store.bll.validators.ProductValidator;
import Store.bll.validators.Validator;
import Store.dao.ProductDAO;
import Store.model.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Bejan Diana 30223
 * Product Business Logic Layer
 */
public class ProductBLL {
    private ProductDAO productDAO;
    private Validator<Product> validator;

    public ProductBLL() {
        validator = new ProductValidator();
        productDAO = new ProductDAO();
    }

    public Product createProduct(ArrayList<String> prop) {
        Product product = new Product(prop.get(0), prop.get(1), Integer.parseInt(prop.get(2)), Integer.parseInt(prop.get(3)), (prop.get(4)));
        return product;

    }

    public void insertProduct(Product product) {
        if (product.getProductID().length() > 0)
            try {
                productDAO.insert(product);
            } catch (IllegalAccessException iee) {
                iee.printStackTrace();
            }
    }

    public Product findProductByID(String clientID) {
        Product client = productDAO.selectById(clientID);

        return client;
    }

    public List<Product> selectAll() {
        List<Product> cl = new ArrayList<Product>();
        cl = productDAO.selectAll();

        return cl;
    }

    public void deleteProduct(int i) {

        productDAO.delete(i);


    }

    public void updateProduct(Product product) {

        productDAO.update(product);


    }
}
