package Store.bll;

import Store.bll.validators.ClientValidator;
import Store.bll.validators.Validator;
import Store.dao.ClientDAO;
import Store.model.Client;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @Author Bejan Diana
 * Client Business Logic Layer
 * Aceasta clasa reprezinta conexiunea dintre model.Client si dao.ClientDAO,
 * Metodele din clasa apeleaza metodele de acces a bazei de date.
 */

public class ClientBLL {
    private ClientDAO clientDAO;
    private Validator<Client> validator;

    public ClientBLL() {
        validator = new ClientValidator();
        clientDAO = new ClientDAO();
    }

    /**
     * Primeste ca parametru un String ce reprezinta un clientID si trimite cerere la baza de date
     * pentru a selecta clientul din baza de date ce satisface cererea
     *
     * @param clientID
     * @return
     */
    public Client findClientByID(String clientID) {
        Client client = clientDAO.selectById(clientID);
        if (client == null) {
            throw new NoSuchElementException("No such Client");
        }
        return client;
    }

    /**
     * Selecteaza toti Clientii din baza de date si ii returneaza sub forma de
     * ArrayList de obiecet de tip Client
     *
     * @return clientList
     */
    public List<Client> selectAll() {
        List<Client> clientList;
        clientList = clientDAO.selectAll();
        if (clientList.size() == 0) {
            throw new NoSuchElementException("No Clients in database");
        }
        return clientList;
    }

    /**
     * Primeste ca parametri proprietatile selectate din interfata grafica si
     * creeaza un nou client
     *
     * @return client
     */
    public Client createClient(ArrayList<String> prop) {

        Client client = new Client(prop.get(0), prop.get(1), prop.get(2), Integer.parseInt(prop.get(3)), prop.get(4));


        return client;

    }

    public void insertClient(Client client) {
        if (client.getClientID().length() > 0)
            try {
                clientDAO.insert(client);
            } catch (IllegalAccessException iee) {
                iee.printStackTrace();
            }
    }

    public void deleteClient(int i) {

        clientDAO.delete(i);


    }

    public void updateClient(Client client) {

        clientDAO.update(client);
    }
}
